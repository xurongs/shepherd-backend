module gitee.com/xurongs/shepherd-backend

go 1.16

require (
	github.com/boj/redistore v0.0.0-20180917114910-cd5dcc76aeff // indirect
	github.com/gin-gonic/contrib v0.0.0-20201101042839-6a891bf89f19
	github.com/gin-gonic/gin v1.5.0
	github.com/go-sql-driver/mysql v1.6.0 // indirect
	github.com/gorilla/sessions v1.1.3 // indirect
	github.com/jinzhu/gorm v1.9.16
	github.com/magiconair/properties v1.8.5
	github.com/satori/go.uuid v1.2.0
	golang.org/x/sys v0.0.0-20210525143221-35b2ab0089ea // indirect
)
