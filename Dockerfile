FROM golang:1.16 as builder

ENV GO111MODULE=on \
    GOPROXY=https://goproxy.cn,direct

WORKDIR /opt
ADD . .
RUN go get -u golang.org/x/sys
RUN GOOS=linux GOARCH=amd64 go build -tags netgo -o shepherd .
RUN mkdir publish && cp shepherd publish


FROM alpine
WORKDIR /opt
COPY --from=builder /opt/publish .
ENV GIN_MODE=release \
    PORT=8081

VOLUME ["/opt/shepherd.properties"]

EXPOSE 8081
ENTRYPOINT ["./shepherd"]