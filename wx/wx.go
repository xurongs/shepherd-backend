package wx

import (
	"encoding/json"
	"fmt"
	"github.com/magiconair/properties"
	"io/ioutil"
	"net/http"
)

type Wx struct {
	appid string
	secret string
}

type WxSession struct {
	OpenId     string `json:"openid"`
	SessionKey string `json:"session_key"`
	UnionId    string `json:"unionid"`
	ErrCode    int    `json:"errcode"`
	ErrMsg     string `json:"errmsg"`
}

func NewWx(props *properties.Properties) *Wx {
	return &Wx{
		appid:  props.MustGetString("wx.appid"),
		secret: props.MustGetString("wx.secret"),
	}
}

func (s *Wx) Code2Session(code string) *WxSession {
	url := fmt.Sprintf("https://api.weixin.qq.com/sns/jscode2session?appid=%s&secret=%s&js_code=%s&grant_type=authorization_code", s.appid, s.secret, code)
	resp, err := http.Get(url)
	if err == nil && resp.StatusCode == 200 {
		body, _ := ioutil.ReadAll(resp.Body)
		var wxSession = WxSession{}
		err := json.Unmarshal(body, &wxSession)
		if err == nil {
			return &wxSession
		}
	}
	return nil
}
