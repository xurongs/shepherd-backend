package db

import (
	"github.com/jinzhu/gorm"
	"github.com/satori/go.uuid"
	"time"
)

type User struct {
	gorm.Model
	Name     string `gorm:"size:255;unique"`
	WxOpenId string `gorm:"size:31;unique"`
	CurrentSpace uint
}

type Space struct {
	gorm.Model
	Name string `gorm:"size:255"`
}

type SpaceMember struct {
	gorm.Model
	Space uint `gorm:"index"`
	User uint `gorm:"index"`
	Title string `gorm:"size:15"`
	Role string `gorm:"size:15"`
}

type Homework struct {
	gorm.Model
	Space uint `gorm:"index"`
	Starts time.Time `gorm:"index;type:date"`
	Ends time.Time `gorm:"index;type:date"`
	Context string `gorm:"type:text"`
	State string `gorm:"size:15"`
}

type Invite struct {
	Token string `gorm:"size:36;primary_key"`
	Space uint
	SendFrom uint
	ApplyTo uint
	Validity time.Time `gorm:"type:datetime"`
}

func (database *Database) GetWxUser(openId string) *User {
	user := User {
		Name: "wx_" + openId,
		WxOpenId: openId,
	}

	if err := database.db.Where("wx_open_id = ?", openId).First(&user).Error; err != nil {
		if err := database.db.Create(&user).Error; err != nil {
			return nil
		}
	}
	return &user
}

func (database *Database) CreateInvite(user uint, space uint, validity time.Time) string {
	token := uuid.NewV4().String()

	invite := Invite { Space: space, Token: token, SendFrom: user, ApplyTo: 0, Validity: validity}
	database.db.Create(&invite)

	return token
}


type InviteInfo struct {
	Token string `json:"Token"`
	SpaceId uint `json:"SpaceId"`
	SpaceName string `json:"SpaceName"`
	SendFrom uint `json:"SendFrom"`
	ApplyTo uint `json:"ApplyTo"`
	Validity time.Time `json:"Validity"`
	Valid bool `json:"Valid"`
}

func (database *Database) GetInvite(token string) *InviteInfo {
	var inviteInfo InviteInfo

	if err := database.db.
		Table("invite").
		Select("invite.token as token, invite.space as space_id, space.name as space_name, invite.send_from as send_from, invite.apply_to as apply_to, invite.validity as validity").
		Joins("left join space on invite.space = space.id").
		Where("invite.token = ?", token).
		Scan(&inviteInfo).Error; err != nil {
			return nil
	} else {
		inviteInfo.Valid = inviteInfo.ApplyTo == 0 && time.Now().Before(inviteInfo.Validity)
	}

	return &inviteInfo
}

func (database *Database) AddMember(space uint, user uint) *SpaceMember {
	var members []SpaceMember
	if err := database.db.Where("space = ? and user = ?", space, user).Find(&members).Error; err != nil {
	} else {
		if len(members) == 0 {
			member := SpaceMember{Space: space, User: user, Role: "guest"}
			database.db.Create(member)
			if err := database.db.Create(&member).Error; err != nil {
				return nil
			}
			return &member
		}
	}
	return nil
}

func (database *Database) AcceptInvite(token string, user uint) *Invite {
	var invite Invite

	if err := database.db.Where("Token = ?", token).First(&invite).Error; err != nil {

	} else {
		if invite.ApplyTo == 0 && time.Now().Before(invite.Validity) {
			invite.ApplyTo = user
			database.db.Save(invite)
			database.AddMember(invite.Space, user)
			database.SetCurrentSpace(user, invite.Space)
			return &invite
		}
	}
	return nil
}

func (database *Database) CopyHomeworks(space uint, date time.Time) []Homework {
	var homework []Homework
	var last Homework

	if err := database.db.Where("Space = ?", space).Order("Starts desc").First(&last).Error; err != nil {
		return nil
	}

	starts := date
	ends := starts.Add(time.Hour * 24)
	if err := database.db.Where("Space = ? AND Starts >= ? AND Ends < ?",
		space, starts.Format("2006/1/2"), ends.Format("2006/1/2")).
		Find(&homework).Error; err != nil {
		return nil
	} else if len(homework) > 0 {
		return nil
	}

	starts2 := last.Starts
	ends2 := starts2.Add(time.Hour * 24)
	if err := database.db.Where("Space = ? AND Starts >= ? AND Ends < ?",
		space, starts2.Format("2006/1/2"), ends2.Format("2006/1/2")).
		Find(&homework).Error; err != nil {
		return nil
	}

	for _, item := range homework {
		var newItem Homework
		newItem.Context = item.Context
		newItem.Starts = date
		newItem.Ends = date
		newItem.Space = space
		database.NewHomework(newItem)
	}
	if err := database.db.Where("Space = ? AND Starts >= ? AND Ends <= ?",
		space, starts.Format("2006/1/2"), ends.Format("2006/1/2")).
		Find(&homework).Error; err != nil {
		return nil
	}

	return homework
}

func (database *Database) GetHomeworks(space uint, date time.Time) []Homework {
	var homework []Homework

	starts := date
	ends := starts.Add(time.Hour * 24)

	if err := database.db.Where("Space = ? AND Starts >= ? AND Ends < ?",
		space, starts.Format("2006/1/2"), ends.Format("2006/1/2")).
		Find(&homework).Error; err != nil {

	}
	return homework
}

func (database *Database) GetHomework(id string) *Homework {
	var homework Homework

	if err := database.db.Where("ID = ?", id).
		First(&homework).Error; err != nil {

	}
	return &homework
}

func (database *Database) NewHomework(homework Homework) *Homework {
	var newItem Homework

	newItem.Space = homework.Space
	newItem.Context = homework.Context
	newItem.Starts = homework.Starts
	newItem.Ends = homework.Ends

	if err := database.db.Create(&newItem).Error; err != nil {
		return nil
	}

	return &newItem
}

func (database *Database) UpdateHomework(id string, homework Homework) *Homework {
	var item Homework

	if err := database.db.Where("ID = ?", id).First(&item).Error; err != nil {

	} else {
		if len(homework.Context) > 0 {
			item.Context = homework.Context
		}
		if !homework.Starts.IsZero() {
			item.Starts = homework.Starts
		}
		if !homework.Ends.IsZero() {
			item.Ends = homework.Ends
		}
		if len(homework.State) > 0 {
			item.State = homework.State
		}
		database.db.Save(item)
	}

	return &item
}

func (database *Database) DeleteHomework(id string) *Homework {
	var item Homework

	if err := database.db.Where("ID = ?", id).First(&item).Error; err != nil {

	} else {
		database.db.Delete(item)
	}

	return &item
}

func (database *Database) GetSapces(userId uint) []Space {
	var spaces []Space

	if err := database.db.
		Joins("left join space_member on space.id = space_member.space").
		Where("space_member.user = ?", userId).
		Find(&spaces).Error; err != nil {

	}
	return spaces
}

func (database *Database) SetCurrentSpace(userId uint, space uint) uint {
	var user User

	if err := database.db.Where("ID = ?", userId).First(&user).Error; err != nil {

	} else {
		if user.CurrentSpace != space {
			user.CurrentSpace = space
			database.db.Save(user)
		}
	}

	return space
}

func (database *Database) GetCurrentSpace(userId uint) uint {
	var user User

	if err := database.db.Where("ID = ?", userId).First(&user).Error; err != nil {

	} else {
		return user.CurrentSpace
	}

	return 0
}

func (database *Database) GetSpaceRole(userId uint, space uint) string {
	var member SpaceMember
	var role string

	if err := database.db.Where("user = ?", userId).
		Where("space = ?", space).First(&member).Error; err != nil {

	} else {
		role = member.Role
	}

	return role
}

type SpaceMemberInfo struct {
	Id uint `json:"ID"`
	UserId uint `json:"UserID"`
	UserName string `json:"UserName"`
	Title string `json:"Title"`
	Role string `json:"Role"`
	Me bool `json:"Me"`
}

func (database *Database) GetSpaceName(spaceId uint64) string {
	var space Space

	if err := database.db.Where("ID = ?", spaceId).
		First(&space).Error; err != nil {

	}

	return space.Name
}

func (database *Database) CreateSpace(userId uint, spaceName string) *Space {
	space := Space{ Name:spaceName }
	database.db.Create(&space)

	member := SpaceMember{ Space: space.ID, User: userId, Role: "owner" }
	database.db.Create(&member)

	return &space
}

func (database *Database) GetSpaceMembers(userId uint, spaceId uint64) []SpaceMemberInfo {
	var members []SpaceMemberInfo

	if err := database.db.
		Table("space_member").
		Select("space_member.id as id, space_member.user as user_id, user.name as user_name, space_member.title as title, space_member.role as role").
		Joins("left join user on user.id = space_member.user").
		Where("space_member.space = ?", spaceId).
		Scan(&members).Error; err != nil {
	} else {
		for index, member := range members {
			members[index].Me = userId == member.UserId
		}
	}

	return members
}

func (database *Database) GetUserName(userId uint) string {
	var user User

	if err := database.db.Where("ID = ?", userId).First(&user).Error; err != nil {

	}
	return user.Name
}

func (database *Database) SetUserName(userId uint, name string) string {
	var user User

	if err := database.db.Where("ID = ?", userId).First(&user).Error; err != nil {

	} else {
		if user.Name != name {
			user.Name = name
			database.db.Save(user)
		}
	}
	return name
}

func (database *Database) SetMemberRole(currentUser uint, space uint64, member uint, role string) string {
	var spaceMember2 SpaceMember

	if err := database.db.Where("space = ? and user = ? and role = ?", space, currentUser, "owner").First(&spaceMember2).Error; err != nil {
	} else {
		var spaceMember SpaceMember
		if err := database.db.Where("space = ? and user = ?", space, member).First(&spaceMember).Error; err != nil {
		} else {
			if spaceMember.Role != role {
				spaceMember.Role = role
				database.db.Save(spaceMember)
			}
			return spaceMember.Role
		}
	}

	return ""
}
