package db

import (
	"fmt"
	gorm "github.com/jinzhu/gorm"
	_ "github.com/jinzhu/gorm/dialects/mysql"
	"github.com/magiconair/properties"
)

type Database struct {
	db *gorm.DB
}

func (database *Database) AutoMigrate() {
	if err := database.db.Set("gorm:table_options", "ENGINE=InnoDB").
		AutoMigrate(&User{}, &Space{}, &SpaceMember{}, &Homework{}, &Invite{}).Error; err != nil {
		panic(err)
	}
}

func NewDatabase(props *properties.Properties) *Database {
	conn := fmt.Sprintf("%s:%s@tcp(%s:%s)/%s?charset=utf8&parseTime=True&loc=Local",
		props.GetString("mysql.user", "shepherd"),
		props.GetString("mysql.password", "shepherd"),
		props.GetString("mysql.host", "127.0.0.1"),
		props.GetString("mysql.port", "3306"),
		props.GetString("mysql.database", "shepherd"),
	)
	dbase, err := gorm.Open("mysql", conn)
	if err != nil {
		panic(err)
	} else {
		// 全局禁用表名复数
		dbase.SingularTable(true) // 如果设置为true,`User`的默认表名为`user`,使用`TableName`设置的表名不受影响
	}

	return &Database{
		db:   dbase,
	}
}

func (database *Database) Create(value interface{}) *gorm.DB {
	return database.db.Create(value)
}
