package main

import (
	"github.com/gin-gonic/contrib/sessions"
	"github.com/gin-gonic/gin"
	"net/http"
	"strconv"
)

func GetSpaces(c *gin.Context) {
	session := sessions.Default(c)
	user := session.Get("user")
	c.JSON(http.StatusOK, gin.H{
		"status": "ok",
		"data": database.GetSapces(user.(uint)),
	})
}

func PostSpace(c *gin.Context) {
	session := sessions.Default(c)
	user := session.Get("user")

	// Parse JSON
	var data struct {
		Name string `json:"name" binding:"required"`
	}

	err := c.Bind(&data)
	if err == nil {
		if user != nil && user.(uint) != 0 {
			space := database.CreateSpace(user.(uint), data.Name)
			c.JSON(http.StatusOK, gin.H{
				"status": "ok",
				"data": gin.H{
					"id": space.ID,
					"name": space.Name,
				},
			})
		}
	}
}

func GetSpaceHelper(c *gin.Context) {
	spaceParam := c.Param("id")
	if spaceParam == "current" {
		GetCurrentSpace(c)
	} else if spaceParam == "current2" {
		GetCurrentSpace2(c)
	} else {
		GetSpace(c)
	}
}

func PutMemberRole(c *gin.Context) {
	session := sessions.Default(c)
	currentUser := session.Get("user")

	// Parse JSON
	var data struct {
		Space uint64 `json:"space" binding:"required"`
		Member uint `json:"member" binding:"required"`
		Role string `json:"role" binding:"required"`
	}

	err := c.Bind(&data)
	if err == nil {
		c.JSON(http.StatusOK, gin.H{
			"status": "ok",
			"data": gin.H{
				"space": data.Space,
				"member": data.Member,
				"role": database.SetMemberRole(currentUser.(uint), data.Space, data.Member, data.Role),
			},
		})
	}
}

func GetSpace(c *gin.Context) {
	session := sessions.Default(c)
	user := session.Get("user")
	spaceId, _ := strconv.ParseUint(c.Param("id"), 10, 0)
	c.JSON(http.StatusOK, gin.H{
		"status": "ok",
		"data": gin.H{
			"space": spaceId,
			"name": database.GetSpaceName(spaceId),
			"members": database.GetSpaceMembers(user.(uint), spaceId),
		},
	})
}

func GetCurrentSpace(c *gin.Context) {
	session := sessions.Default(c)
	space := session.Get("space")
	if space != nil && space.(uint) != 0 {
		c.JSON(http.StatusOK, gin.H{
			"status": "ok",
			"data": space.(uint),
		})
	} else {
		c.JSON(http.StatusNotFound, gin.H{
			"status": "error",
			"message": "no current space",
		})
	}
}

func GetCurrentSpace2(c *gin.Context) {
	session := sessions.Default(c)
	user := session.Get("user")
	space := session.Get("space")
	if space != nil && space.(uint) != 0 {
		c.JSON(http.StatusOK, gin.H{
			"status": "ok",
			"data": gin.H{
				"space": space.(uint),
				"role": database.GetSpaceRole(user.(uint), space.(uint)),
			},
		})
	} else {
		c.JSON(http.StatusNotFound, gin.H{
			"status": "error",
			"message": "no current space",
		})
	}
}

func PutCurrentSpace(c *gin.Context) {
	session := sessions.Default(c)
	user := session.Get("user")

	// Parse JSON
	var data struct {
		ID uint `json:"ID" binding:"required"`
	}

	err := c.Bind(&data)
	if err == nil {
		space := data.ID

		session := sessions.Default(c)
		session.Set("space", space)
		session.Save()

		if space == database.SetCurrentSpace(user.(uint), space) {
			c.JSON(http.StatusOK, gin.H{
				"status": "ok",
				"data": data.ID,
			})
		}
	}
}

func GetProfile(c *gin.Context) {
	session := sessions.Default(c)
	user := session.Get("user")
	if user != nil && user.(uint) != 0 {
		c.JSON(http.StatusOK, gin.H{
			"status": "ok",
			"data": gin.H{
				"id": user.(uint),
				"name": database.GetUserName(user.(uint)),
			},
		})
	} else {
		c.JSON(http.StatusNotFound, gin.H{
			"status": "error",
			"message": "no current user",
		})
	}
}

func PutProfile(c *gin.Context) {
	session := sessions.Default(c)
	user := session.Get("user")

	// Parse JSON
	var data struct {
		Name string `json:"name" binding:"required"`
	}

	err := c.Bind(&data)
	if err == nil {
		if user != nil && user.(uint) != 0 {
			c.JSON(http.StatusOK, gin.H{
				"status": "ok",
				"data": gin.H{
					"id": user.(uint),
					"name": database.SetUserName(user.(uint), data.Name),
				},
			})
		}
	}
}
