package main

import (
	"flag"
	"gitee.com/xurongs/shepherd-backend/db"
	"gitee.com/xurongs/shepherd-backend/wx"
	"github.com/gin-gonic/contrib/sessions"
	"github.com/gin-gonic/gin"
	"github.com/magiconair/properties"
	"log"
	"net/http"
	"time"
)

var props *properties.Properties
var database *db.Database

func setupRouter() *gin.Engine {
	// Disable Console Color
	// gin.DisableConsoleColor()
	r := gin.Default()

	//store := sessions.NewCookieStore([]byte("secret"))
	address := props.GetString("redis.host", "127.0.0.1") + ":" + props.GetString("redis.port", "6379")
	password := props.GetString("redis.password", "")
	secret := props.GetString("session.secret", "secret")
	store, err := sessions.NewRedisStore(10, "tcp", address, password, []byte(secret))
	if err != nil {
		// Handle the error. Probably bail out if we can't connect.
	}
	r.Use(sessions.Sessions("mysession", store))

	// Ping test
	r.GET("/ping", func(c *gin.Context) {
		c.String(http.StatusOK, "pong")
	})

	// Ping test
	r.POST("/wxlogin", func(c *gin.Context) {
		session := sessions.Default(c)

		// Parse JSON
		var data struct {
			Code string `json:"code" binding:"required"`
		}

		if c.Bind(&data) == nil {
			sess := wx.NewWx(props).Code2Session(data.Code)
			session.Set("openid", sess.OpenId)
			session.Set("session", sess.SessionKey)

			user := database.GetWxUser(sess.OpenId)
			session.Set("user", user.ID)
			session.Set("space", user.CurrentSpace)

			session.Save()

			// The user credentials was found, set user's id to key AuthUserKey in this context, the user's id can be read later using
			// c.MustGet(gin.AuthUserKey).
			c.Set(gin.AuthUserKey, user.ID)
		}
	})

	r.GET("invite/:id", func(c *gin.Context) {
		id := c.Param("id")

		c.JSON(http.StatusOK, gin.H{
			"status": "ok",
			"data": database.GetInvite(id),
		})
	})

	r.POST("invite/:id/join", func(c *gin.Context) {
		id := c.Param("id")
		session := sessions.Default(c)
		user := session.Get("user")
		if user != nil {
			invite := database.AcceptInvite(id, user.(uint))
			if invite != nil {
				currentSpace := database.GetCurrentSpace(user.(uint))
				if currentSpace != 0 {
					session.Set("space", currentSpace)
					session.Save()
				}

				c.JSON(http.StatusOK, gin.H{
					"status": "ok",
					"data": invite,
				})
			} else {
				c.JSON(http.StatusBadRequest, gin.H{
					"status": "error",
					"message": "invalid invite",
				})
			}
		}
	})

	r.GET("profile", GetProfile)
	r.PUT("profile", PutProfile)
	r.PUT("role", PutMemberRole)

	space := r.Group("space", func(c *gin.Context) {
		// check session
	})

	space.GET("/", GetSpaces)
	space.POST("/", PostSpace)
	space.PUT("current", PutCurrentSpace)
	space.GET(":id", GetSpaceHelper)

	currentSpace := r.Group("/", func(c *gin.Context) {
		// check session
		session := sessions.Default(c)
		space := session.Get("space")
		if space != nil && space.(uint) != 0 {
			//
		} else {
			c.JSON(http.StatusBadRequest, gin.H{
				"status": "error",
				"message": "no current space",
			})
		}
	})

	currentSpace.POST("invite", func(c *gin.Context) {
		session := sessions.Default(c)
		space := session.Get("space")
		user := session.Get("user")
		if space != nil && user != nil {

			today := time.Now()

			c.JSON(http.StatusOK, gin.H{
				"status": "ok",
				"data": database.CreateInvite(user.(uint), space.(uint), today.Add(time.Hour * 24)),
			})
		}
	})

	currentSpace.POST("homework/copy", func(c *gin.Context) {
		session := sessions.Default(c)
		space := session.Get("space")
		if space != nil {

			today := time.Now()
			copyToString := c.DefaultQuery("to", today.Format("2006/1/2"))
			copyTo, _ := time.Parse("2006/1/2", copyToString)

			c.JSON(http.StatusOK, gin.H{
				"status": "ok",
				"data": database.CopyHomeworks(space.(uint), copyTo),
			})
		}
	})

	currentSpace.GET("homework", func(c *gin.Context) {
		session := sessions.Default(c)
		space := session.Get("space")
		if space != nil {

			today := time.Now()
			dateString := c.DefaultQuery("date", today.Format("2006/1/2"))
			date, _ := time.Parse("2006/1/2", dateString)

			c.JSON(http.StatusOK, gin.H{
				"status": "ok",
				"data": database.GetHomeworks(space.(uint), date),
			})
		} else {
			c.JSON(http.StatusNotFound, gin.H{
				"status": "error",
				"message": "no current space",
			})
		}
	})

	currentSpace.GET("homework/:id", func(c *gin.Context) {
		session := sessions.Default(c)
		space := session.Get("space")
		if space != nil {

			id := c.Param("id")

			c.JSON(http.StatusOK, gin.H{
				"status": "ok",
				"data": database.GetHomework(id),
			})
		} else {
			c.JSON(http.StatusNotFound, gin.H{
				"status": "error",
				"message": "no current space",
			})
		}
	})

	currentSpace.PUT("homework/:id", func(c *gin.Context) {
		session := sessions.Default(c)
		space := session.Get("space")

		// Parse JSON
		var data struct {
			Context string `json:"context"`
			Starts string `json:"starts"`
			Ends string `json:"ends"`
			State string `json:"state"`
		}

		if c.Bind(&data) == nil {

			id := c.Param("id")

			var homework db.Homework

			starts, _ := time.ParseInLocation("2006/1/2", data.Starts, time.Local)
			ends := starts

			homework.Space = space.(uint)
			homework.Context = data.Context
			homework.Starts = starts
			homework.Ends = ends
			homework.State = data.State

			newItem := database.UpdateHomework(id, homework)
			c.JSON(http.StatusOK, gin.H{
				"status": "ok",
				"data": newItem,
			})
		}
	})

	currentSpace.DELETE("homework/:id", func(c *gin.Context) {
		//session := sessions.Default(c)
		//space := session.Get("space")

		id := c.Param("id")

		newItem := database.DeleteHomework(id)
		c.JSON(http.StatusOK, gin.H{
			"status": "ok",
			"data": newItem,
		})
	})

	currentSpace.POST("homework", func(c *gin.Context) {
		session := sessions.Default(c)
		space := session.Get("space")

		// Parse JSON
		var data struct {
			Context string `json:"context" binding:"required"`
			Starts string `json:"starts" binding:"required"`
			Ends string `json:"ends"`
		}

		if c.Bind(&data) == nil {
			var homework db.Homework

			starts, _ := time.ParseInLocation("2006/1/2", data.Starts, time.Local)
			ends := starts

			homework.Space = space.(uint)
			homework.Context = data.Context
			homework.Starts = starts
			homework.Ends = ends

			if err := database.Create(&homework).Error; err != nil {
				c.JSON(http.StatusInternalServerError, gin.H{
					"status": "error",
					"message": "Create homework failed",
				})
			} else {
				c.JSON(http.StatusOK, gin.H{
					"status": "ok",
					"data": homework,
				})
			}
		}
	})

	return r
}

func main() {
	flag.Parse()

	// properties
	propertiesFile := flag.String("config", "shepherd.properties", "the configuration file")
	log.Printf("[shepherd][info] Loading configuration from [%s]", *propertiesFile)
	var err error
	if props, err = properties.LoadFile(*propertiesFile, properties.UTF8); err != nil {
		log.Fatalf("[shepherd][error] Unable to read properties:%v\n", err)
	}

	// database
	database = db.NewDatabase(props)
	database.AutoMigrate()

	// router
	r := setupRouter()

	addr := props.GetString("http.server.host", "127.0.0.1") + ":" + props.GetString("http.server.port", "8080")
	r.Run(addr)
}
